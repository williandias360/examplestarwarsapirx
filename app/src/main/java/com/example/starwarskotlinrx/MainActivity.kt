package com.example.starwarskotlinrx

import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.ListView
import androidx.appcompat.app.AppCompatActivity
import com.example.starwarskotlinrx.model.api.StarWarsApi
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class MainActivity : AppCompatActivity() {

    var listView: ListView? = null
    var movies = mutableListOf<String>()
    var movieAdapter: ArrayAdapter<String>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        listView = ListView(this)
        setContentView(listView)
        movieAdapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, movies)

        listView?.adapter = movieAdapter

        val api = StarWarsApi()
        api.loadMoviesFull()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                movies.add("${it.title} - ${it.episodeId} \n ${it.characters}")
            }, {
                it.printStackTrace()
            }, {
                movieAdapter?.notifyDataSetChanged()
            })
    }
}
