package com.example.starwarskotlinrx.model.api

import android.net.Uri
import android.util.Log
import com.example.starwarskotlinrx.model.Character
import com.example.starwarskotlinrx.model.Movie
import com.google.gson.GsonBuilder
import io.reactivex.Observable
import io.reactivex.functions.BiFunction
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class StarWarsApi {
    val service: StarWarsApiDef
    val peopleCache = mutableMapOf<String, Person>()

    init {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY


        val httpClient = OkHttpClient.Builder()
        httpClient.addInterceptor(logging)

        val gson = GsonBuilder().setLenient().create()

        val retrofit = Retrofit.Builder()
            .baseUrl("http://swapi.co/api/")
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .client(httpClient.build())
            .build()

        service = retrofit.create(StarWarsApiDef::class.java)
    }


    fun loadMovies(): Observable<Movie> {
        return service.listMovies()
            .flatMap { filmResults -> Observable.fromIterable(filmResults.results) }
            .flatMap { film -> Observable.just(Movie(film.title, film.episodeId, ArrayList())) }

    }

    fun loadMoviesFull(): Observable<Movie> {
        return service.listMovies()
            .flatMap { filmResults -> Observable.fromIterable(filmResults.results) }
            .flatMap { film ->
                Observable.zip(
                    Observable.just(Movie(film.title, film.episodeId, ArrayList())),
                    Observable.fromIterable(film.personUrls)
                        .flatMap { personUrl ->
                            Observable.concat(
                                getCache(personUrl),
                                service.loadPerson(Uri.parse(personUrl).lastPathSegment).doOnNext {
                                    Log.d("NGVL", "DOWNLOAD--->$it")
                                    peopleCache.put(personUrl, it)
                                }
                            ).firstElement().toObservable()
                        }
                        .flatMap { person ->
                            Observable.just(Character(person.name, person.gender))
                        }.toList().toObservable(),
                    BiFunction<Movie, MutableList<Character>, Movie> { movie, characters ->
                        movie.characters.addAll(characters)
                        movie
                    }
                )
            }
    }

    private fun getCache(personUrl: String): Observable<Person> {
        return Observable.fromIterable(peopleCache.keys)
            .filter { key -> key == personUrl }
            .flatMap {
                Log.d("NGVL", "CACHE--->$it")
                Observable.just(peopleCache[personUrl])
            }
    }
}